# AMA discussion
## Q1.What is the call stack in JS?
The call stack in JavaScript is a mechanism for managing function invocations. When a function is called, it is added to the top of the stack. If that function calls another function, the new function is added to the top of the stack. When a function completes, it is removed from the top of the stack, and control returns to the function below it. This stack-based structure helps manage the execution order of functions and is crucial for understanding how JavaScript handles synchronous and asynchronous operations.

## Q2. what is the use case of async and await in JS?
async and await are used in JavaScript to handle asynchronous operations more cleanly and efficiently. They allow you to write asynchronous code that looks and behaves like synchronous code, making it easier to read and maintain. The async keyword is used to declare an asynchronous function, and await is used to pause the execution of the async function until a Promise is resolved. Common use cases include fetching data from APIs, reading files, or any operation that involves waiting for a response without blocking the main thread.

## Q3.what is document.querySelector and document.querySelectorAll()?
document.querySelector and document.querySelectorAll are DOM methods in JavaScript used to select elements from the DOM.
- `document.querySelector` returns the first element within the document that matches the specified selector.
- `document.querySelectorAll` returns a NodeList of all elements within the document that match the specified selector.

These methods are powerful because they allow you to use CSS-style selectors to target elements.

## Q4.  when do we use .get() Function in python?
In Python, the .get() function is commonly used with dictionaries. It retrieves the value associated with a specified key. If the key is not found, it returns None (or a default value if specified). This method is useful for avoiding KeyError exceptions and for providing default values when a key is missing.

## Q5. How async and await works in JS?
async and await work by leveraging Promises to handle asynchronous operations in JavaScript.
- `async`: When a function is declared with async, it automatically returns a Promise. If the function returns a value, the Promise is resolved with that value. If the function throws an error, the Promise is rejected with that error.
- `await`: The await keyword can only be used inside async functions. It pauses the execution of the async function, waits for the Promise to resolve, and returns the resolved value. If the Promise is rejected, await throws the rejected value.

This mechanism makes asynchronous code look and behave like synchronous code, simplifying error handling and improving readability.

## Q6.What are WHERE clause and HAVING clause used for?
### In SQL:
- `WHERE` clause is used to filter records before any groupings are made. It specifies conditions on individual rows.
```sql
SELECT * FROM employees WHERE age > 30;
```
- `HAVING` clause is used to filter records after groupings are made. It specifies conditions on groups created by the GROUP BY clause.
```sql
SELECT department, COUNT(*) FROM employees GROUP BY department HAVING COUNT(*) > 10;
```


## Q7.how to read csv data in python?
in Python, CSV data can be read using the csv module or pandas library.

- Using csv module:
```python
import csv
with open('data.csv', mode='r') as file:
    csv_reader = csv.reader(file)
    for row in csv_reader:
        print(row)
```

## Q8. Event delegation in JS?
Event delegation is a technique in JavaScript where a single event listener is added to a parent element to manage events for multiple child elements. Instead of attaching individual event listeners to each child, the parent element's event listener can handle events triggered by any of its children by using the event object's target property.

```javascript
document.querySelector('#parent').addEventListener('click', function(event) {
    if (event.target && event.target.matches('button')) {
        console.log('Button clicked:', event.target);
    }
});
```
This is efficient and reduces memory usage.

## Q9.What is a git conflict?
A git conflict occurs when changes from different branches cannot be automatically merged by Git. This typically happens when two or more commits modify the same line of a file or when one branch deletes a file that another branch has modified. Git marks the conflicted areas in the affected files, and the user must manually resolve the conflicts by editing the files and committing the resolved changes.

## Q10. What is the difference between em, rem, and % in CSS?
In CSS, em, rem, and % are units used to define sizes relative to other sizes.
- `em`: Relative to the font-size of the element it is used on. If no font-size is specified, it inherits from the parent element.
- `rem`: Relative to the font-size of the root element (typically <html>).
- `%`: Relative to the parent element's size for width, height, padding, margin, etc.