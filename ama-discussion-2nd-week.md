# AMA discussion
## 1.What is the diff b/w Func declaration, func statement and func expression?
- ***Function Declaration***:
    - Syntax: Defined using the function keyword followed by the function name.
    - Hoisting: Can be called before its declaration because it is hoisted.
    - Naming: Must have a name.
```javascript

// Function declaration
function greet() {
    console.log('Hello, world!');
}
greet(); // Output: 'Hello, world!'
```
- ***Function Statement***:
    - Syntax: Defined by assigning a function to a variable.
    - Hoisting: Not hoisted, so it cannot be called before its definition.
    - Naming: Can be anonymous or named.

```javascript
const greet = function() {
    console.log('Hello, world!');
};

greet();
```
-  ***Function Expression***:
    - Syntax: A function expression with a name.
    - Hoisting: Not hoisted, so it cannot be called before its definition.
    - Naming: The name is available only within the function's scope.

```js
const greet = () => {
    console.log('Hello, world!');
};

greet(); // Output: 'Hello, world!'
```

## 2.what is Schema in DBMs?
A schema in DBMS (Database Management System) refers to the structure that defines the organization of data in a database. It includes definitions of tables, columns, data types, constraints, relationships, and other elements. A schema acts as a blueprint for how the data is stored and organized.


## 3.What is JSON.stringify?
JSON.stringify is a method in JavaScript that converts a JavaScript object or value to a JSON string. This is useful for serializing data to be sent over the network or stored in a string format.

```javascript
const obj = { name: "John", age: 30 };
const jsonString = JSON.stringify(obj);
console.log(jsonString); // '{"name":"John","age":30}'
```

## 4.What is self in python?
In Python, self is a reference to the instance of the class. It is used within a class method to access instance variables and other methods of the class. It is the first parameter of instance methods in Python.

```python
class MyClass:
    def __init__(self, name):
        self.name = name
    
    def greet(self):
        print(f"Hello, {self.name}!")
```


## 5.What is difference between Promise.race() and promise.any()?
- ***Promise.race()***: This method returns a promise that resolves or rejects as soon as one of the promises in the iterable resolves or rejects.

```javascript
const promise1 = new Promise((resolve) => setTimeout(resolve, 500, 'one'));
const promise2 = new Promise((resolve) => setTimeout(resolve, 100, 'two'));

Promise.race([promise1, promise2]).then((value) => {
  console.log(value); // "two"
});
```
- ***Promise.any()***: This method returns a promise that resolves as soon as one of the promises in the iterable fulfills. If all the promises are rejected, it rejects with an AggregateError.
```javascript

const promise1 = new Promise((_, reject) => setTimeout(reject, 500, 'one'));
const promise2 = new Promise((resolve) => setTimeout(resolve, 100, 'two'));

Promise.any([promise1, promise2]).then((value) => {
  console.log(value); // "two"
});
```
## 6.How do you define Async function in JS?
An async function is defined using the async keyword before the function declaration. This allows the use of await within the function to wait for asynchronous operations.

```javascript

async function fetchData() {
    const response = await fetch('https://api.example.com/data');
    const data = await response.json();
    console.log(data);
}
```
## 7.What is Async and await in JS?
- **async**: This keyword is used to declare an asynchronous function that returns a promise. Inside this function, you can use await.
await: This keyword is used to pause the execution of an async function until a promise is resolved or rejected. It can only be used inside an async function.

```javascript
async function example() {
    const value = await somePromise();
    console.log(value);
}
```

## 8.How to consume promise?
we can consume a promise using then, catch, and finally methods or using async/await syntax.

```javascript

// Using then, catch, finally
somePromise()
    .then(result => console.log(result))
    .catch(error => console.error(error))
    .finally(() => console.log('Done'));

// Using async/await
async function consumePromise() {
    try {
        const result = await somePromise();
        console.log(result);
    } catch (error) {
        console.error(error);
    } finally {
        console.log('Done');
    }
}
```

## 9.How to traverse a object via Higher order function?
You can use higher-order functions like Object.keys, Object.values, and Object.entries to traverse an object.

```javascript

const obj = { a: 1, b: 2, c: 3 };

// Using Object.keys
Object.keys(obj).forEach(key => {
    console.log(`${key}: ${obj[key]}`);
});

// Using Object.values
Object.values(obj).forEach(value => {
    console.log(value);
});

// Using Object.entries
Object.entries(obj).forEach(([key, value]) => {
    console.log(`${key}: ${value}`);
});
```

## 10.Difference between micro task queue and call back queue?
- ***Microtask Queue***: This queue handles microtasks, which are tasks that are executed after the currently executing script and before the event loop continues. Examples include promises' .then() handlers and MutationObserver callbacks.

- ***Callback Queue (Macro task Queue)***: This queue handles macrotasks, which are tasks scheduled to be executed on the next iteration of the event loop. Examples include setTimeout, setInterval, and I/O operations.