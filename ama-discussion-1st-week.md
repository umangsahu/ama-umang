# AMA discussion

## What is init in python?
__init__ is a special method known as the constructor. It is automatically called when a new instance (object) of a class is created. The __init__ method allows you to initialize the attributes (variables) of an object.

``` python
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

# Creating an instance of the Person class
person1 = Person("Alice", 30)

# Accessing attributes
print(person1.name)  # Output: Alice
print(person1.age)   # Output: 30
```

## What is Polymorphism?
Polymorphism, in the context of object-oriented programming, refers to the ability of different classes to be treated as objects of a common superclass. It allows objects of different classes to be treated uniformly based on their common interface, thus enabling code reuse and flexibility.

There are two main types of polymorphism:
- Compile-time polymorphism
- Run-time polymorphism

## What are regular expression?
Regular expressions (regex) in Python are a powerful tool for pattern matching and text manipulation. They allow you to search for patterns within strings, extract specific information, and perform various types of text manipulation based on those patterns.

**Python's re module provides support for regular expressions**

## What is pass keyword in python?
pass is commonly used in situations where a block of code is syntactically required, such as in loops, conditional statements, or function definitions, but you don't want to perform any action within that block.
```python
def some_function():
    # This function is not implemented yet
    pass

if condition:
    # Placeholder for future code
    pass

for item in some_list:
    # Placeholder for processing each item
    pass

```

## What is pep8 in python?
PEP 8 is a document that provides various guidelines to write the readable in Python. PEP 8 describes how the developer can write beautiful code. It was officially written in 2001 by Guido van Rossum, Barry Warsaw, and Nick Coghlan. The main aim of PEP is to enhance the readability and consistency of code.

##  what command we use in git to delete a commit message?
 git reset --soft

## Difference between immutable and mutable data types in python?
Data types in Python are categorized into mutable and immutable data types. Mutable data type is those whose values can be changed, whereas immutable data type is one in which the values can’t be changed.

- **Mutable Data Type** -  List, Dictionaries, and Set

- **Immutable Data Type** - String and Tuples

## Types of argument in function?
In Python, there are several types of arguments that can be passed to a function. These different types provide flexibility in how arguments are passed and used within functions. 

main types of arguments in Python:
- Positional Arguments
- Keyword Arguments
- Default Arguments
- Variable-length Positional Arguments 

##  What is the significance of head pointer in git?
The head pointer in Git indicates the latest commit on the current branch, guiding operations like commits, merges, and rebases.

##  What are different types of union and condition in Sql.
There are two type of operation is present:
- UNION
- UNIONALL

Conditions for Union Operations:
1. Each Table Used with UNION Must Have the Same Number of Columns
2. Columns Must Have the Same Data Type

